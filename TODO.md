+ Find multi-dimensional time-series dataset
+ Build model
  + AutoEncoder
  + VAE
  + Seq2Seq
+ Evaluation method
  + Threshold-based: max/min resconstruction error of normal training data
  + Mahalanobis distance
  + POT of EVT
+ Classical machine learning for AD
  + SVM
+ Build OmniAnomaly by add Planar NF after encoder
