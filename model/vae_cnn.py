import torch
from torch import nn, optim
from torch.nn import parameter

# Fallen layer hidden variable
class Flatten(nn.Module):
  """
  This class is for flattening tensor from (N, ..., ...) to (N, -1)
  """
  def forward(self, x):
    """
    Forward function

    Args:
        x: size (N, ..., ...)
    Return:
        faltten_tensor: (N, -1)
    """
    N = x.shape[0]
    return x.view(N, -1)

class Flatten_inverse(nn.Module):
  """
  Inverse a flatten tensor
  """
  def __init__(self, shape):
    """
    Init

    Args:
        shape: shape to rehape tensor to
    Return:
        None: None
    """
    super().__init__()
    self.shape = shape
  def forward(self, x):
    """
    Forward function

    Args:
        x: input tensor
    Return:
        output: reshaped tensor
    """
    # print(x.shape)
    # print('x ', x[0])
    # print(x.view(self.shape).shape)
    # print('reshaped: ', x.view(self.shape)[0])
    return x.view(self.shape)

# Residual Block for Encoder and Decoder
class ResidualBasicBlock(nn.Module):
  """
  Basic residual block in Resnet
  blocks: Input -> [conv1d/convTrans pose1d (3) -> LeakyReLU] x 2 -> features
  shortcut: Input -> [conv1d (1)] -> residual
  output = feature + residual
  """
  def __init__(self, 
               in_channels, 
               out_channels, 
               downsampling=False,
               upsampling=False):
    """
    Init function for Residual block.
    I turn off all batchnorm for better 

    Args:
        in_channels : int, number of channel in the input of block
        out_channels: int, numver of channel in the output of block
        downsampling: bool, if True, ouptut_size = input_size / 2  
        upsampling  : bool, if True, output_Size = input_size * 2
    Resturn:
        None: None
    """
    super().__init__()
    self.in_channels, self.out_channels = in_channels, out_channels
    self.upsampling, self.downsampling = upsampling, downsampling
    if self.downsampling:
      self.conv1 = nn.Conv1d(in_channels, 
                        out_channels, 
                        kernel_size=3,
                        stride=2,
                        padding=1
      )
    elif self.upsampling:
      self.conv1 = nn.ConvTranspose1d(in_channels,
                                out_channels,
                                kernel_size=2,
                                stride=2,
                                output_padding=0
      )
    else:
      self.conv1 = nn.Conv1d(in_channels, 
                        out_channels, 
                        kernel_size=3,
                        stride=1,
                        padding=1
      )
      
    self.conv2 = nn.Conv1d(
        out_channels,
        out_channels,
        kernel_size=3,
        stride=1,
        padding=1
    )


    self.blocks = nn.Sequential(
      self.conv1,
      # nn.BatchNorm1d(int(out_channels/2)),
      nn.LeakyReLU(),
      self.conv2,
      # nn.BatchNorm1d(out_channels),
      nn.LeakyReLU()
    )
    if self.downsampling:
      self.shortcut = nn.Sequential(
          nn.Conv1d(self.in_channels, 
                    self.out_channels, 
                    kernel_size=1,
                    stride= 2, 
                    padding=0
                    )
          # nn.BatchNorm1d(self.out_channels)
      )    
    elif self.upsampling:
      self.shortcut = nn.Sequential(
          nn.ConvTranspose1d(self.in_channels, 
                              self.out_channels, 
                              kernel_size=1,
                              stride= 2,
                              output_padding=1,
                    )
          # nn.BatchNorm1d(self.out_channels)
      )
    else:
      self.shortcut = nn.Sequential(
          nn.Conv1d(self.in_channels, 
                    self.out_channels, 
                    kernel_size=1,
                    stride= 1, 
                    padding=0
                    )
          # nn.BatchNorm1d(self.out_channels)
      )   

  def forward(self, x):
    """
    Forward function

    Args:
        x: input tensor N x C_in x L
    Return:
        output_tensor: N x C_out x L
    """
    residual = self.shortcut(x)
    x_in = self.blocks(x)
    x_in += residual
    return x_in

# Encoder
class Encoder(nn.Module):
  """
  Encoder network for VAE
  This module encode input into scalar of 2*hidden, 2 parameters (muy, sugma) 
  for each schastic variable.

  Input -> gateNetwork -> blocks -> output
  gateNetwork: 1 conv1d kernel_size = 3
  blocks network is described by arguments in init function:
  Input -> [Residual Blocks] x N -> Flatten -> Linear -> Leaky -> Linear -> output

  """
  def __init__(self, in_channels, hidden_channels, downsampling, hidden_var, L):
    """
    Init function

    Args:
        in_channels: int, number of channel of input
        hidden_channels: list, number of channel in the hidden class
        downsampling: list, if True, the layer downsample by 2
        hidden_var: int, number of hidden variable to encode
        L: length of input
    Return:
        None: None
    """
    super().__init__()
    self.in_channels, self.hidden_channels = in_channels, hidden_channels
    self.downsampling, self.hidden_var = downsampling, hidden_var
    self.L = L
    for b in self.downsampling:
      if b:
        self.L = int(self.L / 2)

    self.gate = nn.Sequential(
        nn.Conv1d(
            self.in_channels,
            self.hidden_channels[0],
            kernel_size = 3,
            stride = 1,
            padding = 1
        )
        # nn.BatchNorm1d(self.hidden_channels[0])
    )
    self.blocks = nn.ModuleList([
            *[ResidualBasicBlock(hidden_channels[i], 
                              hidden_channels[i+1],
                              downsampling=self.downsampling[i])
            for i in range(len(hidden_channels) - 1)],
            Flatten(),
            nn.Linear(in_features  = self.L*hidden_channels[-1], 
                      out_features = self.L*hidden_channels[-1]),
            nn.LeakyReLU(),
            nn.Linear(in_features  = self.L*hidden_channels[-1], 
                      out_features = 2*self.hidden_var)
        ]
    )
  def forward(self, x):
    """
    Forward function

    Args:
        x: input of size N x C_in, L_in
    Return:
        output: output of size N x (2*hidden_var)
    """
    x_in = self.gate(x)
    for block in self.blocks:
      x_in = block(x_in)
    return x_in

# Decoder
class Decoder(nn.Module):
  """
  Decoder for VAE

  Input -> gateNetwork -> blocks -> output
  gatNetwork: Linear -> Leaky -> Linear -> flatten_inverse -> conv1d (3)
  blocks: describe by argument in init function
  """
  def __init__(self, out_channels, hidden_channels, upsampling, hidden_var, L):
    """
    Init function

    Args:
        out_channels: int, number of channel to decode
        hidden_channels: list, number of channel in the hidden class
        downsampling: list, if True, the layer downsample by 2
        hidden_var: int, number of hidden variable to encode
        L: length of input
    Return:
        None: None
    """
    super().__init__()
    self.out_channels, self.hidden_channels = out_channels, hidden_channels
    self.upsampling, self.hidden_var = upsampling, hidden_var
    self.L = L
    for b in upsampling:
      if b:
        self.L = int(self.L/2)

    self.gate = nn.Sequential(
        nn.Linear(in_features  = hidden_var, 
                  out_features = hidden_channels[0]*self.L),
        nn.LeakyReLU(),
        nn.Linear(in_features  = hidden_channels[0]*self.L, 
                  out_features = hidden_channels[0]*self.L),
        Flatten_inverse(shape = (-1, hidden_channels[0], self.L)),
        nn.Conv1d(
            self.hidden_channels[0],
            self.hidden_channels[0],
            kernel_size = 3,
            stride = 1,
            padding = 1
        )
        # nn.BatchNorm1d(self.hidden_channels[0])
    )
    self.blocks = nn.ModuleList(
        [
         *[ResidualBasicBlock(hidden_channels[i], 
                            hidden_channels[i+1],
                            upsampling=self.upsampling[i])
         for i in range(len(hidden_channels) - 1)],
          nn.Conv1d(
            self.hidden_channels[-1],
            self.out_channels,
            kernel_size = 1,
            stride = 1,
            padding = 0
        )
        ]
    )
  def forward(self, x):
    """
    Forwar function

    Args:
        x: Input tensor of shape N x H
        H is the number of hidden variable
    Return:
        otuput: Output tensor of shape N x C_out x L_out
    """
    x_in = self.gate(x)
    for block in self.blocks:
      x_in = block(x_in)
    return x_in

# VAE
class VAE(nn.Module):
  """
  Variable AutoEncoder network

  """
  def __init__(
      self, 
      in_channels,
      hidden_var, 
      hidden_channels, 
      up_down_sampling, 
      L_in):
    """
    Init function

    Args:
        in_channels: int, number of channel of input
        hidden_var: int, number of variable in the middle of network
        hidden_channels: list, list of channel of the hidden features
        up_down_sampling: list, if an element is True, that corresponding layer 
          in encoder is downsampling and in decoder is upsampling
        L_in: Lengh of input
    Return:
        None: None
    """
    super().__init__()
    self.L_in, self.hidden_channels = L_in, hidden_channels
    self.up_down_sampling, self.hidden_var = up_down_sampling, hidden_var
    self.in_channels = in_channels
    
    self.encoder = Encoder(
      self.in_channels,
      self.hidden_channels,
      self.up_down_sampling,
      self.hidden_var,
      self.L_in
    )
    for parameter in self.encoder.parameters():
      nn.init.normal_(parameter, mean=0, std=0.1)
    self.decoder = Decoder(
      self.in_channels,
      self.hidden_channels[::-1],
      self.up_down_sampling[::-1],
      self.hidden_var,
      self.L_in
    )
    for parameter in self.decoder.parameters():
      nn.init.normal_(parameter, mean=0, std=0.1)

  def reparameter(self, x, device):
    """
    Reparameter Trick

    Args:
        x: Tensor of N x (2*hidden_var)
    Return:
        output: Sampled variable
    """
    self.muy = x[:, 0:self.hidden_var]
    self.log_var = x[:, self.hidden_var:]
    self.epsilon = torch.empty(*self.log_var.size(), 
                              device=device
                              ).normal_(0.0, 1.0)
    # print(self.epsilon)
    # print(self.epsilon.shape)
    print()
    return self.muy + self.log_var*self.epsilon

    # return x
    

  def forward(self, x, device):

    h = self.encoder(x)
    z = self.reparameter(h, device)
    x_hat = self.decoder(z)

    return x_hat, self.muy, self.log_var