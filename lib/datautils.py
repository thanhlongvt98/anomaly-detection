# Plot data
# %matplotlib inline

import warnings
import itertools
import pandas
import math
import sys
import os
import numpy as np
import torch
from plotly.offline import download_plotlyjs, plot, iplot

def name2file(dataname):
    name2path = {
        "realAdExchange": [
            "data/NAB/data/realAdExchange/exchange-2_cpc_results.csv",
            "data/NAB/data/realAdExchange/exchange-2_cpm_results.csv"
        ],
        "realTweets" : [
            "./data/NAB/data/realTweets/Twitter_volume_AAPL.csv",
            "./data/NAB/data/realTweets/Twitter_volume_AMZN.csv",
            "./data/NAB/data/realTweets/Twitter_volume_CRM.csv",
            "./data/NAB/data/realTweets/Twitter_volume_CVS.csv",
            "./data/NAB/data/realTweets/Twitter_volume_FB.csv",
            "./data/NAB/data/realTweets/Twitter_volume_GOOG.csv",
            "./data/NAB/data/realTweets/Twitter_volume_IBM.csv",
            "./data/NAB/data/realTweets/Twitter_volume_KO.csv",
            "./data/NAB/data/realTweets/Twitter_volume_PFE.csv",
            "./data/NAB/data/realTweets/Twitter_volume_UPS.csv"
        ]
    }
    return name2path[dataname]


def plotdata(dataname):

    data_file_list = name2file(dataname)
    for data_file in data_file_list:
        Error = None
        if os.path.isfile(data_file):
            dataframe = pandas.read_csv(data_file)
        else:
            Error = "No such file : "+data_file
            print(Error)
        if set(['timestamp','value']).issubset(dataframe.columns) and Error is None:
            x = np.array(dataframe['timestamp'])
            print('Number of samples in ', data_file ,': ', x.shape[0])
            y = np.array(dataframe['value'])
            mean = np.mean(y)

            trace = {"x": x,
                    "y": y,
                    "mode": 'lines',
                    "name": 'Value'}
            trace_mean = {"x": x,
                            "y": np.ones(len(x))*mean,
                            "mode": 'lines',
                            "name": 'Mean'}
            traces = [trace,trace_mean]
            layout = dict(title = "Data plot : "+data_file,
                            xaxis = dict(title = 'X'),
                            yaxis = dict(title = 'Value')
                        )
            fig = dict(data=traces, layout=layout)
            iplot(fig)
            
        else:
            if Error is None:
                Error = "Missing colomns in file "+data_file
            print(Error)

def get_data(dataname):
    data_file_list = name2file(dataname)
    X = None
    for data_file in data_file_list:
        Error = None
        if os.path.isfile(data_file):
            dataframe = pandas.read_csv(data_file)
        else:
            Error = "No such file : "+data_file
            print(Error)
        if set(['timestamp','value']).issubset(dataframe.columns) and Error is None:
            x = np.array(dataframe['value'])
            print('Number of samples in ', data_file, ' : ', x.shape[0])

            if X == None:
                X = torch.from_numpy(x.reshape(1,1,-1).astype(np.float32))
            else:
                X = torch.cat((X, torch.from_numpy(x.reshape(1, 1, -1).astype(np.float32))), dim=1)
        else:
            if Error is None:
                Error = "Missing colomns in file "+data_file
            print(Error)

    return torch.tensor(X)